package com.org.entity;


	import javax.persistence.Column;
import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

	@Entity
	@Table(name="property_details")
	public class PropertyEntity {
		@Id
		@GenericGenerator(name="reg_auto",strategy = "increment")
		@GeneratedValue(generator = "reg_auto")
		@Column(name="id")
	    private Long id;
		@Column(name="name")
	    private String name;
		@Column(name="location")
	    private String location;
		@Column(name="price")
	    private double price;
		@Column(name="description")
	    private String description;
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		@Override
		public String toString() {
			return "PropertyEntity [id=" + id + ", name=" + name + ", location=" + location + ", price=" + price
					+ ", description=" + description + ", getId()=" + getId() + ", getName()=" + getName()
					+ ", getLocation()=" + getLocation() + ", getPrice()=" + getPrice() + ", getDescription()="
					+ getDescription() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
					+ super.toString() + "]";
		}

	    
	}


