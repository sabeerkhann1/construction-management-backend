package com.org.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.org.entity.PropertyEntity;

@Repository
public class ConstructionRepo {

	@Autowired
	private SessionFactory sessionFactory;
	
	public PropertyEntity save(PropertyEntity propertyEntity) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		PropertyEntity savedObject=(PropertyEntity) session.merge(propertyEntity);
		
		transaction.commit();
		return savedObject;
	}
	
	public List<PropertyEntity> findAll() {
		Session session = sessionFactory.openSession();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("from PropertyEntity");
		Query query = session.createQuery(stringBuilder.toString());
		return query.getResultList();
		
	}
//	
//	public PropertyEntity findById(long id) {
//		Session session = sessionFactory.openSession();
//		StringBuilder stringBuilder = new StringBuilder();
//		stringBuilder.append("from PropertyEntity where id=:id");
//		Query query = session.createQuery(stringBuilder.toString());
//		query.setParameter("id", id);
//		return (PropertyEntity) query.uniqueResult();
//		
//	}
//	public List<PropertyEntity> getAppsByType(String appType) {
//		Session session = sessionFactory.openSession();
//		StringBuilder stringBuilder = new StringBuilder();
//		stringBuilder.append("from PropertyEntity where appType=:t");
//		Query query = session.createQuery(stringBuilder.toString());
//		query.setParameter("t", appType);
//		return (List<PropertyEntity>) query.uniqueResult();
//		
//	}
}
