package com.org.service;

import java.util.List;

import com.org.dto.PropertyDto;
import com.org.dto.PropertyResponseDto;
import com.org.entity.PropertyEntity;

import com.org.repository.ConstructionRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class ConstructionManagementImpl implements ConstructionManagement {
	@Autowired
	private ConstructionRepo constRepo;

//	@Override
//	public List<PropertyDto> getAllProperties() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//





	@Override
	public PropertyResponseDto createProperty(PropertyDto propertyDto) {
		
			PropertyResponseDto propertyResponseDto = null;
			PropertyEntity propertyEntity = new PropertyEntity();
			propertyEntity.setName(propertyDto.getName());
			propertyEntity.setLocation(propertyDto.getLocation());
			propertyEntity.setDescription(propertyDto.getDescription());
			propertyEntity.setPrice(propertyDto.getPrice());

			try {
				PropertyEntity savedObj = constRepo.save(propertyEntity);
				if (savedObj == null) {
					new PropertyResponseDto("FAILURE", "500", null, null);
				}
				propertyResponseDto = new PropertyResponseDto("SUCESS", "200", savedObj, null);
			} catch (Exception e) {
				propertyResponseDto = new PropertyResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
			}
			return propertyResponseDto;
		}

	@Override
	public PropertyResponseDto getAll() {
//		return constRepo.findAll();
		return new PropertyResponseDto("SUCESS", "200", constRepo.findAll(), null);
		
		
	}






//	@Override
//	public PropertyDto getAll() {
//		// TODO Auto-generated method stub
//		return null;
//	}
	}
	
	


