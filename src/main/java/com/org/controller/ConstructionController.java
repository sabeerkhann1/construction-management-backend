package com.org.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.org.dto.PropertyDto;
import com.org.dto.PropertyResponseDto;
import com.org.entity.PropertyEntity;
import com.org.service.ConstructionManagementImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ConstructionController {
   @Autowired
    private ConstructionManagementImpl constructionManagementImpl;

   
    @PostMapping("/propertiesP")
    public @ResponseBody PropertyResponseDto create(@RequestBody PropertyDto propertyDto) {
		//requestBody is used for converting  JSON object to java object
		//responseBody converts Java Obj to Json data	
		return constructionManagementImpl.createProperty(propertyDto);
    }

	
	    @GetMapping("/propertiesG")
    public PropertyResponseDto getAllProperties() {
        return constructionManagementImpl.getAll();
    }
//
//    @GetMapping("/getById/{id}")
//    public PropertyDto getPropertyById(@PathVariable String id) {
//        return constructionManagement.getPropertyById(id);
//    }
//
//    @PostMapping("/add")
//    public void addProperty(@RequestBody PropertyDto property) {
//        constructionManagement.addProperty(property);
//    }
//
//    @PutMapping("/update")
//    public void updateProperty(@RequestBody PropertyDto property) {
//        constructionManagement.updateProperty(property);
//    }
}
